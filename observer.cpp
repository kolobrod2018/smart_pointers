// Наблюдатель (Observer)

#include <iostream>
#include <list>
#include <memory>

enum class Lang
{
    ru, en
};

class Observer 
{
public:
    virtual void update(Lang lang) = 0;
};

class Language 
{
    Lang lang{Lang::ru};
    std::list<std::weak_ptr<Observer>> subs;
public:

    Language()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }        

    ~Language()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

    void subscribe(const std::shared_ptr<Observer> &obs) 
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        subs.push_back(obs);
    }

    void unsubscribe(std::weak_ptr<Observer> obs) 
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        // Отсюда https://stackoverflow.com/a/10120851
        subs.remove_if([obs](const auto &p)
            { 
                return !(p.owner_before(obs) || obs.owner_before(p));
            });

        // https://stackoverflow.com/q/28338978
    }

    void set_language(Lang lang_) 
    {
        lang = lang_;
        notify();
    }

    void notify() 
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        for (auto s : subs) 
        {
            if(auto sp = s.lock())
                sp->update(lang);
        }
    }
    
};

class report_observer : public Observer, public std::enable_shared_from_this<report_observer>
{
private:    
    std::weak_ptr<Language> lang;
    report_observer() = delete;
    report_observer(const std::weak_ptr<Language> &l) : lang(l) 
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
public:
    static std::shared_ptr<report_observer> create(const std::weak_ptr<Language> &l)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        auto ret = std::shared_ptr<report_observer>(new report_observer(l));
        l.lock()->subscribe(ret);
        return ret;
    }

    ~report_observer() 
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        // https://stackoverflow.com/q/28338978

        // auto tsp = shared_from_this();
        // if(auto sp = lang.lock())
        //     sp->unsubscribe(tsp);
    }

    void update(Lang l) override 
    {
        std::cout << "switch report template to lang " << int(l) << std::endl;
    }
};

class ui_observer : public Observer, public std::enable_shared_from_this<ui_observer>
{
private: 
    std::weak_ptr<Language> lang;
    ui_observer() = delete;
    ui_observer(std::weak_ptr<Language> l) : lang(l)
    {
    }
public:

    static std::shared_ptr<ui_observer> create(std::weak_ptr<Language> l)
    {
        auto ret = std::shared_ptr<ui_observer>(new ui_observer(l));
        l.lock()->subscribe(ret);
        return ret;
    }

    ~ui_observer()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        // https://stackoverflow.com/q/28338978

        // std::weak_ptr<Observer> tsp = shared_from_this();
        // if(auto sp = lang.lock())
        //     sp->unsubscribe(tsp);
    }

    void update(Lang l) override 
    {
        std::cout << "refresh ui for lang " << int(l) << std::endl;
    }
};

int main(int, char *[]) 
{
    try
    {
        std::cout << "step 1" << std::endl;
        auto lang = std::make_shared<Language>();
        std::cout << "step 2" << std::endl;
        auto rpt = report_observer::create(lang);
    
        {
            std::cout << "step 21" << std::endl;
            auto ui = ui_observer::create(lang);
            std::cout << "step 3" << std::endl;
            lang->set_language(Lang::ru);
            std::cout << "step 4" << std::endl;
        }
    
        lang->set_language(Lang::en);
        std::cout << "step 5" << std::endl;
        auto tsp = rpt->shared_from_this();
        lang->unsubscribe(tsp);
        std::cout << "step 6" << std::endl;
        lang->set_language(Lang::ru);
    }
    catch(const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
