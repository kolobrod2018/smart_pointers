// Внедрение зависимости (Dependency Injection)

#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <memory>

class IStorage
{
public:
    virtual void execute_query() = 0;
};

class Report
{
    std::weak_ptr<IStorage> storage;

public:
    Report(std::weak_ptr<IStorage> storage_) : storage(storage_)
    {
    }

    void print()
    {
        storage.lock()->execute_query();
        std::cout << "done" << std::endl;
    }
};

class TestStorage : public IStorage
{
    int par;

    TestStorage() = delete;
    TestStorage(int p):par(p){} 
public:    

    static auto create(int p=0)
    {
        return std::shared_ptr<TestStorage>(new TestStorage(p));
    }

    void execute_query() override
    {
        std::cout << "... fetching data (" << par << ")" << std::endl;
    }
};


int main(int, char *[])
{
    auto stor = TestStorage::create(1);
    Report report(stor);
    report.print();

    Report rep(std::shared_ptr<TestStorage>(nullptr));
    rep.print();

    return 0;
}
